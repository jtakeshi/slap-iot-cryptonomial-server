/*
 * Copyright (C) 2011-2018 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

//#include <stdarg.h>
//#include <stdio.h>      /* vsnprintf */
#include <string>
//#include <string.h>
#ifndef NENCLAVE
# include <sgx_trts.h> /* sgx_read_rand */
# include "Enclave.h"
# include "Enclave_t.h"  /* print_string */
#endif
#include <vector>
#include <map>
#include <cassert>
#include <cstdint>
#include <climits>

using namespace std;
using uint128_t = unsigned __int128;

static map<uint64_t, vector<uint64_t> > user_to_key;


void init_keys(uint64_t * moduli, size_t num_moduli, unsigned int num_users){
  user_to_key.clear();
  for(unsigned int i = 0; i < num_users; i++){
    vector<uint64_t> v(num_moduli);
#ifndef NENCLAVE
    sgx_read_rand((unsigned char *) v.data(), v.size() * sizeof(uint64_t));
#endif
    for(size_t j = 0; j < v.size(); j++){
#ifdef NENCLAVE      
      v[j] = rand();
      v[j] <<= sizeof(uint64_t)*8/2;
      v[j] |= rand();
#endif      
      v[j] %= moduli[j];
    }
    user_to_key[i] = v;  
  }
  return;
}

uint64_t add_dl_noise_noreduce(const uint64_t input, const uint64_t modulus){
  uint64_t e = 0;
#ifndef NENCLAVE  
  sgx_read_rand((unsigned char *) e, 1);
#else
  e = rand();
#endif  
  e %= 3;
  uint64_t e_64 = (e != 2) ? e : modulus-1;
  uint64_t ret = input + e_64;
  return ret;
} 

void fault_recover(unsigned int * faulted_users, size_t num_faulted_users, uint64_t * moduli, size_t num_moduli, uint64_t * results){
  vector<uint128_t> sums(num_moduli, 0);
  for(size_t i = 0; i < num_faulted_users; i++){
    //Technically not the safest, but we're not moving memory around
    const vector<uint64_t> & user_key = user_to_key[i];
    for(size_t j = 0; j < num_moduli; j++){
      sums[j] += add_dl_noise_noreduce(user_key[j], moduli[j]);
    }
  
  }
  for(size_t j = 0; j < num_moduli; j++){
    results[j] = sums[j] % moduli[j];
  }
  return;
}
