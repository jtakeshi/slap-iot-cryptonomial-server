import sys
import getopt
import random







def main():
  argflags = "n:i:m:f:"
  argflags_long = ["num_users", "iterations", "modulus", "fault"]
  moduli = []
  num_users = 0
  iterations = 0
  fault_proportion = 0.0
  try:
    opts, args = getopt.getopt(sys.argv[1:], argflags, argflags_long)
  except getopt.GetoptErr as err:
    print(err)
    return
  for o, a in opts:
    if o in ["-n", "--num_users"]:
      num_users = int(a)
    elif o in ["-i", "--iterations"]:
      iterations = int(a)
    elif o in ["-m", "--moduli"]:
      moduli.append(int(a))
    elif o in ["-f", "--fault"]:
      fault_proportion = float(a)  
      
  assert num_users >= 2
  assert iterations >= 1
  assert len(moduli) > 0
  for m in moduli:
    assert m > 1      
  assert fault_proportion >= 0.0 and fault_proportion < 1.0  
  
  random.seed()
  
  #NB the constant reappending is inefficient, but that's ok for input generation
  for i in range(iterations):
    outstr = ""
    for j in range(num_users):
      if random.random() > fault_proportion:  # Include this user, if they don't fault
        outstr += str(j) + " "
        for modulus in moduli:
          user_val = random.randrange(modulus)
          outstr += str(user_val) + " "
        outstr += " "  
    print(outstr)      
       
    
    
if __name__ == '__main__':
  main()
