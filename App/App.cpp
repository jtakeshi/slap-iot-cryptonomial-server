/*
 * Copyright (C) 2011-2018 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <vector>
#include <unistd.h>
#include <pwd.h>
#define MAX_PATH FILENAME_MAX
#define BUFFER_SIZE 1200000
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <fstream>
//#include <openssl/evp.h>
#include <chrono>
#include <cstdint>
#include <climits>

#include "Aggregator_Coeff.h"

using namespace std;
using uint128_t = unsigned __int128;


const static float SCALE_DEFAULT = 0.5f;
const static uint64_t PLAIN_MODULUS_BITS_DEFAULT = 16;

/*
#define HASH_LEN 32
#define STRING_LEN 32
#define SIZEOF_SEED 4
*/

#ifndef NENCLAVE

#include "sgx_urts.h"
#include "App.h"
#include "Enclave_u.h"

/* Global EID shared by multiple threads */
sgx_enclave_id_t global_eid = 0;

typedef struct _sgx_errlist_t {
    sgx_status_t err;
    const char *msg;
    const char *sug; /* Suggestion */
} sgx_errlist_t;

/* Error code returned by sgx_create_enclave */
static sgx_errlist_t sgx_errlist[] = {
    {
        SGX_ERROR_UNEXPECTED,
        "Unexpected error occurred.",
        NULL
    },
    {
        SGX_ERROR_INVALID_PARAMETER,
        "Invalid parameter.",
        NULL
    },
    {
        SGX_ERROR_OUT_OF_MEMORY,
        "Out of memory.",
        NULL
    },
    {
        SGX_ERROR_ENCLAVE_LOST,
        "Power transition occurred.",
        "Please refer to the sample \"PowerTransition\" for details."
    },
    {
        SGX_ERROR_INVALID_ENCLAVE,
        "Invalid enclave image.",
        NULL
    },
    {
        SGX_ERROR_INVALID_ENCLAVE_ID,
        "Invalid enclave identification.",
        NULL
    },
    {
        SGX_ERROR_INVALID_SIGNATURE,
        "Invalid enclave signature.",
        NULL
    },
    {
        SGX_ERROR_OUT_OF_EPC,
        "Out of EPC memory.",
        NULL
    },
    {
        SGX_ERROR_NO_DEVICE,
        "Invalid SGX device.",
        "Please make sure SGX module is enabled in the BIOS, and install SGX driver afterwards."
    },
    {
        SGX_ERROR_MEMORY_MAP_CONFLICT,
        "Memory map conflicted.",
        NULL
    },
    {
        SGX_ERROR_INVALID_METADATA,
        "Invalid enclave metadata.",
        NULL
    },
    {
        SGX_ERROR_DEVICE_BUSY,
        "SGX device was busy.",
        NULL
    },
    {
        SGX_ERROR_INVALID_VERSION,
        "Enclave version was invalid.",
        NULL
    },
    {
        SGX_ERROR_INVALID_ATTRIBUTE,
        "Enclave was not authorized.",
        NULL
    },
    {
        SGX_ERROR_ENCLAVE_FILE_ACCESS,
        "Can't open enclave file.",
        NULL
    },
};

/* Check error conditions for loading enclave */
void print_error_message(sgx_status_t ret)
{
    size_t idx = 0;
    size_t ttl = sizeof sgx_errlist/sizeof sgx_errlist[0];

    for (idx = 0; idx < ttl; idx++) {
        if(ret == sgx_errlist[idx].err) {
            if(NULL != sgx_errlist[idx].sug)
                printf("Info: %s\n", sgx_errlist[idx].sug);
            printf("Error: %s\n", sgx_errlist[idx].msg);
            break;
        }
    }
    
    if (idx == ttl)
    	printf("Error code is 0x%X. Please refer to the \"Intel SGX SDK Developer Reference\" for more details.\n", ret);
}

/* Initialize the enclave:
 *   Step 1: try to retrieve the launch token saved by last transaction
 *   Step 2: call sgx_create_enclave to initialize an enclave instance
 *   Step 3: save the launch token if it is updated
 */
int initialize_enclave(void)
{
    char token_path[MAX_PATH] = {'\0'};
    sgx_launch_token_t token = {0};
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;
    int updated = 0;
    
    /* Step 1: try to retrieve the launch token saved by last transaction 
     *         if there is no token, then create a new one.
     */
    /* try to get the token saved in $HOME */
    const char *home_dir = getpwuid(getuid())->pw_dir;
    
    if (home_dir != NULL && 
        (strlen(home_dir)+strlen("/")+sizeof(TOKEN_FILENAME)+1) <= MAX_PATH) {
        /* compose the token path */
        strncpy(token_path, home_dir, strlen(home_dir));
        strncat(token_path, "/", strlen("/"));
        strncat(token_path, TOKEN_FILENAME, sizeof(TOKEN_FILENAME)+1);
    } else {
        /* if token path is too long or $HOME is NULL */
        strncpy(token_path, TOKEN_FILENAME, sizeof(TOKEN_FILENAME));
    }

    FILE *fp = fopen(token_path, "rb");
    if (fp == NULL && (fp = fopen(token_path, "wb")) == NULL) {
        printf("Warning: Failed to create/open the launch token file \"%s\".\n", token_path);
    }

    if (fp != NULL) {
        /* read the token from saved file */
        size_t read_num = fread(token, 1, sizeof(sgx_launch_token_t), fp);
        if (read_num != 0 && read_num != sizeof(sgx_launch_token_t)) {
            /* if token is invalid, clear the buffer */
            memset(&token, 0x0, sizeof(sgx_launch_token_t));
            printf("Warning: Invalid launch token read from \"%s\".\n", token_path);
        }
    }
    /* Step 2: call sgx_create_enclave to initialize an enclave instance */
    /* Debug Support: set 2nd parameter to 1 */
    ret = sgx_create_enclave(ENCLAVE_FILENAME, SGX_DEBUG_FLAG, &token, &updated, &global_eid, NULL);
    if (ret != SGX_SUCCESS) {
        print_error_message(ret);
        if (fp != NULL) fclose(fp);
        return -1;
    }

    /* Step 3: save the launch token if it is updated */
    if (updated == FALSE || fp == NULL) {
        /* if the token is not updated, or file handler is invalid, do not perform saving */
        if (fp != NULL) fclose(fp);
        return 0;
    }

    /* reopen the file with write capablity */
    fp = freopen(token_path, "wb", fp);
    if (fp == NULL) return 0;
    size_t write_num = fwrite(token, 1, sizeof(sgx_launch_token_t), fp);
    if (write_num != sizeof(sgx_launch_token_t))
        printf("Warning: Failed to save launch token to \"%s\".\n", token_path);
    fclose(fp);
    return 0;
}

/* OCall functions */
void ocall_print_string(const char *str)
{
    /* Proxy/Bridge will check the length and null-terminate 
     * the input string to prevent buffer overflow. 
     */
    printf("%s", str);
}

#else

#include "../Enclave/Enclave.h"

#endif

#ifndef NENCLAVE
/* Application entry */
int SGX_CDECL main(int argc, char ** argv){
  (void)(argc);
  (void)(argv);
#else
int main(int argc, char ** argv){
#endif  

  steady_clock::time_point start, end;

#ifndef NENCLAVE
  start = steady_clock::now();
  /* Initialize the enclave */
  if(initialize_enclave() < 0){
      printf("Enter a character before exit ...\n");
      getchar();
      return -1; 
  }
  end = steady_clock::now();
  cout << "Enclave_init(s)," << duration_cast<std::chrono::nanoseconds>(end-start).count() << endl;
#endif  

  vector<uint64_t> aggregator_key;
  uint64_t t = 1;
  unsigned int log_t = PLAIN_MODULUS_BITS_DEFAULT;
  unsigned int num_users = 0;
  vector<uint64_t> moduli;
  double faulting_users_proportion = 0.0023;
  bool generate = false;
  long double BETA = 0.00009079985;
  unsigned int num_iters_guess = 100;
  unsigned int polynomial_modulus_degree = 0;

  int c;
  while ((c = getopt(argc, argv, "t:n:m:vgi:N:")) != -1){
    switch(c){
      case 't':{
        log_t = atoi(optarg);
        break;
      }
      //Iterations are determined by the number of lines of input
      //But, send in a guess please
      case 'i':{
        num_iters_guess = atoi(optarg);
        break;
      }
      case 'n':{
        num_users = atoi(optarg);
        break;
      }
      case 'N':{
        polynomial_modulus_degree = atoi(optarg);
        break;
      }
      case 'm':{
        uint64_t m;
        string s = optarg;
        stringstream ss(s);
        ss >> m;
        moduli.push_back(m);
        break;
      }
      case 'g':{
        generate = !generate;
        break;
      }
      default:{
        cout << "ERROR: unrecognized input: " << c << endl;
        return 0;
      }
    }
  }

  assert(num_users);
  unsigned int log_num_users = ceil(log2(num_users));
  unsigned int practical_plaintext_space = log_t + log_num_users;
  assert(practical_plaintext_space < 64);
  t <<= practical_plaintext_space;
  uint64_t clear_space = 1;
  clear_space <<= log_t;
  

  //Each line of input has a space-separated set of user inputs, 
  // consisting of their user ID in [num_users] (unsigned int), followed by num_moduli inputs (uint64)

  assert(num_users);  

  //Generate parameters and print some moduli, then exit
  if(generate){
    //Choose for SLAP_NS
    unsigned int ctext_bits = ceil(log2(num_users) 
      + log_t + LOG2_3);
    Parameters ctext_parms(ctext_bits);
    
    unsigned int num_moduli = 0;
    unsigned int mod_bits_found = 0;
    size_t mod_idx = 0;
    while(mod_bits_found < ctext_bits){
      assert(mod_idx < ctext_parms.moduli_count());
      num_moduli++;
      mod_bits_found += ceil(log2(ctext_parms.moduli(mod_idx)));
    }
    
    cerr << "Plaintext space: " << log_t << endl;
    cerr << "Requested bitsize " << ctext_bits << endl;
    cerr << "Printing " << num_moduli << " moduli, " << mod_bits_found << " total bits" << endl;
    size_t deg = ctext_parms.poly_mod_degree();
    cout << deg << endl;
    for(size_t i = 0; i < num_moduli; i++){
      assert(ctext_parms.moduli(i));
      cout << ctext_parms.moduli(i) << ' ';
    }
    cout << endl;
    return 0;
  }

  assert(moduli.size());
  assert(polynomial_modulus_degree);

  start = steady_clock::now();

  Parameters ctext_parms(polynomial_modulus_degree, moduli, true);

  //Create all the transition parameters, agg. keys, etc.
  Aggregator_Coeff agg(ctext_parms, t, SCALE_DEFAULT, 
                 num_users, NS, 
                 BETA);

  end = steady_clock::now();
  cout << "lattice_param_gen," << duration_cast<std::chrono::nanoseconds>(end-start).count() << endl;

  start = steady_clock::now();
  vector<Polynomial> agg_keys = agg.public_keys(0xDEADBEEF, num_iters_guess);
#ifndef NENCLAVE
  //Initialize enclave keys
  init_keys(global_eid, moduli.data(), moduli.size(), num_users);
#else
  init_keys(moduli.data(), moduli.size(), num_users);
#endif  
  end = steady_clock::now();
  cout << "key_gen," << duration_cast<std::chrono::nanoseconds>(end-start).count() << endl;

  unsigned int num_fault_users_guess = ceil(faulting_users_proportion*num_users);
  
  vector<double> read_times, agg_times, fault_times;
  
  string line;
  //User inputs are in RNS, with probably 1-2 moduli

  unsigned int line_count = 0;
  while(getline(cin, line)){
    vector<vector<uint64_t> > inputs(moduli.size());
    for(vector<uint64_t> & v : inputs){
      v.reserve(num_fault_users_guess);
    }
    stringstream ss(line);
    unsigned int user_buf_size = num_users / CHAR_WIDTH;
    if(num_users % CHAR_WIDTH){
      user_buf_size++;
    }
    vector<bool> saw_user(user_buf_size, false);
    unsigned int user_id;
    uint64_t val;
    unsigned int users_seen = 0;
    //First, read in and sum up all the provided user inputs
    start = steady_clock::now();
    while(ss >> user_id){
      assert(user_id < num_users);
      /*
      char offset = 1 << (user_id & (CHAR_BIT - 1));
      unsigned int buffer_idx = user_id / CHAR_BIT;
      */
      if(saw_user[user_id]){
        cerr << "#ERROR: duplicate user " << user_id << " at line " << line_count;
        return 0;
      }
      saw_user[user_id] = true;
      users_seen++;
      for(size_t i = 0; i < moduli.size(); i++){
        ss >> val;
        assert(val < moduli[i]);
        inputs[i].push_back(val);
      }
    }
    end = steady_clock::now();
    double d = duration_cast<std::chrono::nanoseconds>(end-start).count();
    read_times.push_back(d);

    vector<uint128_t> agg_result(moduli.size(), 0);

    //Next, call SGX to recover from the fault
    //Technically the SGX should have read all the input itself too, but the computation time is still accurate
    if(users_seen < num_users){
      start = steady_clock::now();
      
      //First, get a list of faulted users in a nice form to send to the SGX
      vector<unsigned int> missing_users;
      missing_users.reserve(num_users-users_seen);
      for(unsigned int i = 0; i < saw_user.size(); i++){
        if(!saw_user[i]){
          missing_users.push_back(i);
        }
      }
      
      vector<uint64_t> fault_recovered(moduli.size(), 0);
#ifndef NENCLAVE
      fault_recover(global_eid, missing_users.data(), missing_users.size(), moduli.data(), moduli.size(), fault_recovered.data());
#else
      fault_recover(missing_users.data(), missing_users.size(), moduli.data(), moduli.size(), fault_recovered.data());
#endif  
      //Move the results to 128-bit ints
      //SGX only lets us pass up to 64 bits
      //This is the easiest way, and doesn't take long
      for(size_t i = 0; i < agg_result.size(); i++){
        agg_result[i] = fault_recovered[i];
      }

      end = steady_clock::now();
      d = duration_cast<std::chrono::nanoseconds>(end-start).count();
      fault_times.push_back(d);
    }
    
    

    //Do the aggregation, after fault tolerance
    //Choose which key to use
    unsigned int key_to_use = line_count % num_iters_guess;
    //A real implementation would get more keys here if needed, but for these tests this shouldn't be needed,
    //so we'll just make sure we don't go out-of-bounds
    start = steady_clock::now();
    for(size_t i = 0; i < moduli.size(); i++){
      for(size_t j = 0; j < inputs[i].size(); j++){
        agg_result[i] += inputs[i][j];
      }
      agg_result[i] %= moduli[i];
    }
    end = steady_clock::now();
    //NB we can separate aggregation and base conversion time
    d = duration_cast<std::chrono::nanoseconds>(end-start).count();
    //Aggregation done, now time to base convert and decrypt
    double base_conv_time;
    uint64_t result = agg.dec_ns(agg_keys, agg_result, key_to_use, 
      base_conv_time);   
    agg_result[0] = result; //Prevent unused warning
    d += base_conv_time;
    agg_times.push_back(d);
    
    line_count++;
  }

  cout << "read,";
  for(const double d : read_times){
    cout << d << ',';
  }
  cout << endl;

  cout << "agg,";
  for(const double d : agg_times){
    cout << d << ',';
  }
  cout << endl;

  cout << "fault,";
  for(const double d : fault_times){
    cout << d << ',';
  }
  cout << endl;
 
#ifndef NENCLAVE 
  /* Destroy the enclave */
  sgx_destroy_enclave(global_eid);
#endif  
  
  return 0;
}

